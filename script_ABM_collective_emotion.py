import random
import numpy as np
from scipy.stats import truncnorm
import matplotlib.animation as animation
from ComplexNetworkSim import NetworkAgent, Sim
# all the packages
preference_list = []
class ColEmo(NetworkAgent):
    def __init__(self, state, initialiser):
        NetworkAgent.__init__(self, state, initialiser)
        self.preference =  np.random.normal(0,self.globalSharedParameters['preference']) #this is the defree of preference (regulation) each participants has.
        self.gamma = 1 #self.globalSharedParameters['gamma'] # the decay term.
        #fixed variables (for now)
        self.event = self.globalSharedParameters['event'] # rate of experience  - low numbers mean less chance
        self.bump = 0#globalSharedParameters['bump']
        #self.noise = np.random.normal(0.09, 0.15)  # noise term
        self.conform = truncnorm.rvs(0.28,0.6) #np.random.normal(0.28,0.2)  #this is the degree of conformity each participant has.
        self.thresh = -1# np.random.normal(3,0.5)
        #print "thresh %s" %self.thresh
        # define selfstatevector to be similar to state (for the animation)
        self.stateVector = self.state # each participant in this package recieves a variable (state)
        # get local_avg action once to economize on computation

    def Run(self):
        while True:
            self.set_local_avg()
            influence =  (self.state-self.local_avg)*self.conform # certain conform ratio
            """
            if influence >0:
                influence = influence
            else:
                influence = 0
            """
            # I made this cose for situations in which influence is = 0 (when there are no people that express their emotions)
            #print "influence %s" %influence
            emotion = (self.state*self.gamma) - influence + self.preference +np.random.normal(0, 0.15) # the individual emotion
            # this part forms the emotional bump that a participant recieves using the experience ratio.
            self.event = self.event*1
            if self.flip() == "bump": # if the flip condition fit the bump - than we need to bump.
                self.state = emotion + self.bump #  can be changed
            elif self.flip() == "no_bump":
                self.state = emotion
            self.state = sorted([1, self.state, 8])[1] # define the max and min limit.
            # this following part is designed to create the threshold that every person has.
            # we are forming to parallel lists, self state and self-state vector/ Self state vercotr becomes None everytime something doesn't pass
            # a certain threshold - the average of nb is taken of all the numbers that does not include None
            if self.state > self.thresh:
                self.stateVector = self.state
                #print "stateVector %s" %self.stateVector
                #print "state %s" %self.state
            else:
                self.stateVector = None
                #print "stateVector %s" %self.stateVector
                #print "state %s" %self.state
            yield Sim.hold, self, NetworkAgent.TIMESTEP_DEFAULT # This yield function defines how long each agent waits.

    def set_local_avg(self):
        nbrs = self.getNeighbouringAgentsIter()
        a = [nb.stateVector for nb in nbrs]
        #print "neighbor %s" %a
        self.local_avg = np.mean(filter(None, a)) # create a filter the removes nones from average.
        #print "average %s" %self.local_avg

        """
        nbrs = self.getNeighbouringAgentsIter()
        try:
            a = [nb.stateVector for nb in nbrs]
            self.local_avg = np.mean(filter(None, a)) # create a filter the removes nones from average.
            print "average %s" %self.local_avg
            print "neighbor %s" %a
        except RuntimeWarning:
            self.local_avg = self.stateVector
        """

    def flip(self):
        return "no_bump" if random.random() > self.event else "bump"
