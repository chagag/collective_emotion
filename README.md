**Agent-based model of collective emotion.**

From the earliest days of psychology, emotions largely have been conceptualized as individual-level phenomena. 
Even when trying to understand the emotions of groups, psychologists generally have tended to focus on the experience of each individual group member. 
Recently, however, there has been growing interest in exploring emotions at the collective level, treating the group rather than the individual as the unit of analysis. 
This approach asserts that in some cases, there is a unique collective-level property that emerges from a complex set of individual-level interactions. 
If this is true, thinking about emotions at the collective level may provide unique information about a variety of group-related situations


The goal of this model is to explore some of the ideas related to collective emotions using an agent-based modeling. 
Out model is based in principle on this paper  - https://osf.io/bc7e6/


The model is based on the library complexnetoworksim whichrequires python 2.
The model uses networksX as the to create the network inferstructure. 

A new and improved version of the model is coming soon!


